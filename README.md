## Get-WlanProfiles
Básicamente lo que hace este script es "parsear" la salida de los comandos `netsh wlan show profiles` que arroja un listado de los perfiles WiFi del equipo y `netsh wlan show profile 'nombre perfil' key=clear` que muestra informacion de un perfil en especifico.

### Get-WlanProfiles.psm1
Este archivo exporta la funcion `Get-WLAN_Profiles`

#### Get-WLAN_Profiles
Esta funcion recibe un parametro, `$LANGUAGE` que por defecto toma el valor de `$Host.CurrentUICulture.Name`, además solo puede ser `'es-ES'` o `'en-EN'`:
```powershell
param (
  [ValidateSet('es-ES','en-EN')]
  $LANGUAGE = $Host.CurrentUICulture.Name
)
```

Después se declara la variable `$LANGUAGES`, que es un `HashTable` el cual contiene por cada idioma (`es-ES`, `en-EN`), el texto usado para filtrar la salida de los comandos anteriormente mencionados:

```powershell
$LANGUAGES = @{
		'es-ES' = New-Object psobject -Property @{
			'user_profiles_text' 	 = 'Perfil de todos los usuarios'
			'profile_not_found_text' = 'No se encuentra el perfil'
			'ssid_name_text' 		 = 'Nombre de SSID'
			'network_type_text' 	 = 'Tipo de red'
			'authentication_text'	 = 'Autenticaci�n'
			'encryption_text'	 	 = 'Cifrado'
			'key_text' 				 = 'Contenido de la clave'
		}
		'en-EN' = New-Object psobject -Property @{
			'user_profiles_text' 	 = 'All User Profile'
			'profile_not_found_text' = 'No se encuentra el perfil'
			'ssid_name_text' 		 = 'SSID name'
			'network_type_text' 	 = 'Network Type'
			'authentication_text'	 = 'Authentication'
			'encryption_text'	 	 = 'Cipher'
			'key_text' 				 = 'Key Content'
		}
	}
```

Después en la variable `$LANG` se guarda el objeto que corresponda al idioma actual:
```powershell
$LANG = $LANGUAGES.($LANGUAGE)
```

A continuacion se declara la funcion `get-valuebyname` que toma 2 variables:
- `$inputText`: El texto del cual extraer el valor
- `$nameString`: El nombre del campo

Está funcion usa el nombre del campo como una expresion regular y si coincide con el `$inputText`, se reemplaza todo lo que coincida con la expresion regular `^[^:]*:` con un string vacio, que básicamente borra todos los caracteres desde el inicio de la linea hasta que haya un ':' (sin incluirlo)

Ejm:
```powershell
"lorem: ipsum" => ": ipsum"
```

Luego retorna el string resultante quitandole el primer caracter, es decir el ':'

```powershell
function getValueByName ( $inputText, $nameString ) {
  $Value = '';
  if ([regex]::IsMatch($inputText,"\b$nameString\b",'IgnoreCase')) {
    $Value = ([Regex]::Replace($inputText,'^[^:]*:','')); 
    return $Value.Substring(1)
  }
  #return $value.Trim();
}
```

Se inicializa un array vacio en la variable `$Profiles` y se invoca el comando `netsh wlan show profiles`, cuyo resultado se guarda en la variable `$Resultado`:
```powershell
$Profiles = @()
$Result = netsh wlan show profiles
```

Por cada linea del resultado...
```powershell
$Result | % {
```

Nota: El operador `%` es un alias de `For-Each`

Por cada linea del resultado (`$_`), se extrae el valor usando la funcion `getValueByName`, usando como patron/nombre el string del objeto `$LANG` con la clave `user_profiles_text`. Que para el idioma español sería: 'Perfil de todos los usuarios', ya que asi comienzan las lineas donde hay un perfil

Este valor se guarda en la variable `$profile` y si tiene alguno valor (no es nulo), se agrega al array
```powershell
$profile = getValueByName $_ $LANG.'user_profiles_text';
```

Se inicializa un array y un contador:
```powershell
$WLAN_Profiles = @()
$rowNumber = -1
```

Por cada perfil:
```powershell
$Profiles | % {
```

Se guarda en la variable `$wlan_Profile` el resultado de la ejecucion del comando `netsh wlan show profile 'perfil' key=clear`

```powershell
$wlan_Profile = netsh wlan show profile $_ key=clear
```

Si el resultado contiene el texto correspondiente a la clave `'profile_not_found_text'` del idioma seleccionado, se retorna, es decir se pasa al siguiente perfil:
```powershell
if ($wlan_Profile.Contains($LANG.'profile_not_found_text')){
  return
}
```

Se inicaliza la variable `$InterfaceName` y se recorre cada linea del resultado:
```powershell
$InterfaceName = $null
$wlan_Profile | % {
```

Si la variable `$InterfaceName` es nula, es decir, la primera iteracion:
```powershell
if (-not $InterfaceName) {
```

Se extrae el valor de la linea, y si el valor no es nulo y contiene comillas dobles (") se eliminan de los lados usando el metodo `.Trim('"')`:
```powershell
$InterfaceName = getValueByName $_ $LANG.'ssid_name_text'
if ($InterfaceName) {
  if ($InterfaceName.Contains('"')) {
    $InterfaceName = $InterfaceName.Trim('"')
  }
}
```

Se valida nuevamente que la variable `$InterfaceName` no sea nula (lo cual es redundante)

De no ser así, la variable `$row` se inicializa con un objeto con las propiedades `InterfaceName` y `SSID` con el nombre de la interfaz, y las demas propiedades se dejan vacias
```powershell
if ($InterfaceName) {
  $row = New-Object PSObject -Property @{
    InterfaceName = $InterfaceName
    SSID = $InterfaceName
    NetworkType=""
    Authentication=""
    Encryption=""
    Key=""
  }
```
Se aumenta el contador de la fila (para hacerle seguimiento a la posicion de la fila actual), se agrega la fila actual al array `$WLAN_Profiles` y se retorna para continuar con la siguiente linea del resultado
```powershell
$rowNumber+=1
$WLAN_Profiles += $row
#$WLAN_Profiles | ft
return
```

En las iteraciones subsecuentes, se verifica si las diferentes propiedades de la fila son nulas, de ser así, se extrae el valor usando la funcion `getValueByName` y si el valor extraido no es nulo se guarda en la propiedad correspondiente:
```powershell
if (!($WLAN_Profiles[$rowNumber].NetworkType)) {
  $NetworkType = getValueByName $_ $LANG.'network_type_text';
  if ($NetworkType) {
      $WLAN_Profiles[$rowNumber].NetworkType = $NetworkType
  }
}

if (!($WLAN_Profiles[$rowNumber].Authentication)) {
  $Authentication = getValueByName $_ $LANG.'authentication_text';
  if ($Authentication) {
      $WLAN_Profiles[$rowNumber].Authentication = $Authentication
  }
}

if (!($WLAN_Profiles[$rowNumber].Encryption)) {
  $Encryption = getValueByName $_ $LANG.'encryption_text';
  if ($Encryption) {
      $WLAN_Profiles[$rowNumber].Encryption = $Encryption
  }
}

if (!($WLAN_Profiles[$rowNumber].Key)) {
  $Key = getValueByName $_ $LANG.'key_text';
  if ($Key) {
      $WLAN_Profiles[$rowNumber].Key = $Key
  }
}
```


Una vez se ha iterado sobre cada linea de cada perfil, el array `$WLAN_Profiles` contiene la informacion de todos los perfiles.

Se verifica si el contador (tamaño/cantidad) de este array es mayor que 0
```powershell
if ($WLAN_Profiles.Count -gt 0) {
```

De no ser así, se envia el texto 'No WLAN Profiles found!' a la salida estandar de la consola (stdout):
```powershell
'No WLAN Profiles found!' | Out-Host
```

Pero en el caso de que el array tenga datos.

Se envia la cantidad de perfiles encontrados al stdout:
```powershell
'Total WLAN_Profiles: ' + $WLAN_Profiles.Count | Out-Host
```

Se especifica el orden de las propiedades de cada objeto del array:
```powershell
$WLAN_Profiles | Select-Object SSID,
                            Authentication,
                            Key,
                            Encryption,
                            NetworkType
```


### Get-WlanProfiles.bat
Este archivo ejecuta el siguiente script ofuscado:
```powershell
[IO.File]::ReadAllText("$PWD\Get-WlanProfiles.psm1")|IEX;
[IO.File]::WriteAllText("$PWD\WLAN_Profiles.txt",("Author: H3ll
0WORLD - www.facebook.com/HolaMundo.YT`n" + (Get-WLAN_Profiles|Format-Table -Wrap|Out-String)))
```

Que importa el modulo `Get-WlanProfiles.psm1` y escribe al archivo `"$PWD\WLAN_Profiles.txt"` el resultado de invocar la funcion `Get-WLAN_Profiles` formateada como tabla